export default function app(state = [], action) {
  switch (action.type) {
    case "EXAMPLE":
      return {
        ...state,
        example: action.example
      };

    case "EMOJIS_VISIBLE":
      return {
        ...state,
        emojisVisible: true
      };

    case "REGISTER_SORT":
      return {
        ...state,
        sorted: action.sort
      };

    case "UPDATE_SURVEY_RESULTS":
      return {
        ...state,
        surveyData: action.surveyData
      };

    case "REGISTER_MOODS":
      return {
        ...state,
        moods: action.moods
      };

    case "MEDICINE_OR_NOT":
      return {
        ...state,
        medicineOrNot: action.medicineOrNot
      };

    case "LEGAL_OR_NOT":
      return {
        ...state,
        legalOrNot: action.legalOrNot
      };

    case "NETHERLANDS_OR_NOT":
      return {
        ...state,
        netherlandsOrNot: action.netherlandsOrNot
      };

    case "UPDATE_DATA":
      return {
        ...state,
        data: action.data
      };

    case "REGISTER_BASIC_INFO":
      return {
        ...state,
        basicInfoRegistered: action.bool
      };

    case "REGISTER_TRY":
      return {
        ...state,
        tryed: action.tryed
      };

    case "REGISTER_GENDER":
      return {
        ...state,
        gender: action.gender
      };

    case "REGISTER_AGE":
      return {
        ...state,
        age: action.age
      };
    default:
      return state;
  }
}
