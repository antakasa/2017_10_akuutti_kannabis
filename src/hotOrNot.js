import React, { Component } from "react";
import { Button, Icon } from "semantic-ui-react";

class ButtonExampleToggle extends Component {
  state = {};

  handleClick = e =>
    this.setState({ yes: false, no: false, [e]: !this.state.active });

  render() {
    const { yes, no } = this.state;

    return (
      <div>
        <Button
          basic
          toggle
          active={yes}
          size="huge"
          content={this.props.fOption}
          icon="thumbs outline up"
          onClick={() => {
            this.handleClick("yes"), this.props.func("yes");
          }}
        />
        <Button
          basic
          toggle
          active={no}
          size="huge"
          content={this.props.sOption}
          icon="thumbs outline down"
          onClick={() => {
            this.handleClick("no"), this.props.func("no");
          }}
        />
      </div>
    );
  }
}

export default ButtonExampleToggle;
