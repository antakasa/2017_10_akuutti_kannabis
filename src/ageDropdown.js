import React, { Component } from "react";
import Dropdown from "react-dropdown";
import "./ageDropdown.css";
import { Button } from "semantic-ui-react";
import range from "lodash.range";
let options = range(16, 65);
options = options.map(e => e + "-vuotias");
let newOp = ["15-vuotias tai alle", ...options, "65-vuotias tai yli"];

class FlatArrayExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null
    };
    this._onSelect = this._onSelect.bind(this);
  }

  onSave(txt) {
    console.log(txt);
    let numb = txt.match(/\d/g);
    numb = numb.join("");
    this.props.registerAge(parseInt(numb), 10);
  }

  _onSelect(option) {
    this.setState({ selected: option });
    this.onSave(option.label);
  }

  render() {
    const defaultOption = this.state.selected;

    const { selected } = this.state;
    return (
      <section>
        <Dropdown
          options={newOp}
          onChange={this._onSelect}
          value={defaultOption}
          placeholder="Kuinka vanha olet?"
        />
      </section>
    );
  }
}

export default FlatArrayExample;
