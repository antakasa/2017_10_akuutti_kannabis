import React, { Component } from "react";
import forOwn from "lodash.forown";
import inRange from "in-range";
import has from "lodash.has";
import DOMPurify from "dompurify";

const LoopParagraphs = props => {
  if (
    !has(props.data, "yes") &&
    typeof props.data.paragraphs.map !== "function"
  )
    return <NoDataFound />;

  let whichOne = null;
  if (props.yesOrNo) whichOne = props.yesOrNo;
  return has(props.data, "paragraphs")
    ? <div>
        {props.data.paragraphs.map((e, i) => (
          <p
            key={i}
            dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(e.value) }}
          />
        ))}
      </div>
    : <div>
        {typeof props.data[whichOne].map === "function"
          ? props.data[whichOne].map((e, i) => (
              <p
                key={i}
                dangerouslySetInnerHTML={{
                  __html: DOMPurify.sanitize(e.value)
                }}
              />
            ))
          : <p
              dangerouslySetInnerHTML={{
                __html: DOMPurify.sanitize(props.data[whichOne])
              }}
            />}
      </div>;
};

const NoDataFound = () => (
  <div>
    <p>
      Valitettavasti ikäryhmääsi koskevaa tutkittua tietoa ei ole saatavilla. Näet tämän viestin myös siinä tapauksessa, jos et kertonut edellisessä vaiheessa ikääsi.
    </p>
  </div>
);

const ShowContentBasedOnAge = props => {
  let { data, age, yesOrNo } = props;
  if (typeof yesOrNo === "undefined") yesOrNo = null;
  let targetObject;
  forOwn(
    data,
    value =>
      inRange(
        parseInt(age, 10),
        parseInt(value.minAge, 10),
        parseInt(value.maxAge, 10)
      )
        ? (targetObject = <LoopParagraphs data={value} yesOrNo={yesOrNo} />)
        : null
  );

  if (typeof targetObject === "undefined" || targetObject === null)
    targetObject = <NoDataFound />;

  return targetObject;
};
export default ShowContentBasedOnAge;
