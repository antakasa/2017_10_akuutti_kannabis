import { createStore, compose } from "redux";
import rootReducer from "./reducers/index";

export const defaultState = {
  app: {
    age: null,
    emojisVisible: false
  }
};

const store = createStore(
  rootReducer,
  defaultState,
  compose(window.devToolsExtension ? window.devToolsExtension() : f => f)
);

export default store;
