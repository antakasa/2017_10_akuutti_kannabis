import React, { Component } from "react";
import { Button, Icon } from "semantic-ui-react";

class ButtonExampleToggle extends Component {
  state = {};

  handleClick = e =>
    this.setState({
      woman: false,
      man: false,
      other: false,
      [e]: !this.state.active
    });

  render() {
    const { woman, man, other } = this.state;

    return (
      <div>
        <Button
          basic
          toggle
          active={woman}
          size="huge"
          content="Nainen"
          icon="woman"
          onClick={() => {
            this.handleClick("woman"), this.props.func("nainen");
          }}
        />

        <Button
          basic
          toggle
          active={man}
          size="huge"
          content="Mies"
          icon="man"
          onClick={() => {
            this.handleClick("man"), this.props.func("mies");
          }}
        />
        <Button
          basic
          toggle
          active={other}
          size="huge"
          content="Muu"
          icon="genderless"
          onClick={() => {
            this.handleClick("other"), this.props.func("muu");
          }}
        />
      </div>
    );
  }
}

export default ButtonExampleToggle;
