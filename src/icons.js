import React from "react";
import { Header, Icon, Grid } from "semantic-ui-react";
import times from "lodash.times";
import forOwn from "lodash.forown";
const iconCreator = (icon, color, gender) => {
  let genderIcon;
  if (gender === "mies") genderIcon = "male";
  else if (gender === "nainen") genderIcon = "female";
  else genderIcon = icon & 1 ? "female" : "male";
  return (
    <Icon
      style={{
        margin: "4px",
        flex: "0 1 calc(8% - 8px)",
        fontSize: "1.5em",
        color: color
      }}
      name={genderIcon}
    />
  );
};

const pickRightValue = (group, data, gender) => {
  if (gender === "miehistä ja naisista") gender = "data"; // data = both woman and man, for those who answered "other"
  if (gender === "miehistä") gender = "miehet";
  if (gender === "naisista") gender = "naiset";
  let returnvalue;
  forOwn(data, value => {
    value.name === group ? (returnvalue = value[gender]) : null;
  });
  return returnvalue;
};

const genderWord = gender => {
  if (gender === "mies") return "miehistä";
  if (gender === "nainen") return "naisista";
  if (gender === "muu") return "miehistä ja naisista";
};

const IconExampleColored = props => {
  let data = props.data;
  let group = props.group;
  let rightValue = pickRightValue(group, data, genderWord(props.gender));

  return (
    <div>
      <Header as="h4" style={{}}>

        {group}
        -vuotiaista suomalaisista
        {" "}
        {genderWord(props.gender)}
        {" "}
        kannabista on kokeillut noin {" "}
        {rightValue}
        {" "}
        prosenttia.
      </Header>
      <figure>
        <div
          style={{ display: "flex", flexFlow: "row wrap", maxWidth: "500px" }}
        >

          {times(100, i => {
            return i < rightValue
              ? iconCreator(i, "#00809f", props.gender)
              : iconCreator(i, "#dcdcdc", props.gender);
          })}
        </div>

        <figcaption>
          <a
            target="_blank"
            href="http://www.julkari.fi/bitstream/handle/10024/126845/hakkarainen.pdf?sequence=4"
          >
            Lähde{" "}
          </a>
        </figcaption>
      </figure>
      <div>
        {props.gender === "mies"
          ? <p>
              Saman ikäryhmän naisista
              {" "}
              {pickRightValue(group, data, "naiset")}
              {" "}
              sadasta on kokeillut kannabista.
              {" "}
              {" "}
              Miehet ja naiset huomioituna luku on
              {" "}
              {pickRightValue(group, data, "miehistä ja naisista")}.
            </p>
          : null}

        {props.gender === "nainen"
          ? <p>
              Saman ikäryhmän miehistä {" "}
              {pickRightValue(group, data, "miehet")}
              {" "}
              sadasta on kokeillut kannabista.
              Miehet ja naiset huomioituna luku on
              {" "}
              {pickRightValue(group, data, "miehistä ja naisista")}.
            </p>
          : null}

        {props.gender === "muu"
          ? <p>
              Naisista {pickRightValue(group, data, "naiset")}
              {" "}
              sadasta ja miehistä
              {" "}
              {pickRightValue(group, data, "miehet")}
              {" "}
              sadasta tässä ikäryhmässä on kokeillut kannabista.
              {" "}
            </p>
          : null}

      </div>
    </div>
  );
};

export default IconExampleColored;
