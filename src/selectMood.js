import React, { Component } from "react";
import { Button, Icon, Header } from "semantic-ui-react";

import { emojify } from "react-emojione";
import { whichEmoji } from "./emojiStastistics.js";

import forOwn from "lodash.forown";
const options = {
  convertShortnames: true,
  convertUnicode: true,
  convertAscii: true,
  style: {
    height: 20,
    margin: 4
  }
};

const ButtonGenerator = props => {
  return props.obj.optionId !== "15"
    ? <Button
        toggle
        size="huge"
        style={{ marginTop: "10px" }}
        onClick={() => props.func(props.obj.optionId)}
        active={props.active}
        basic
      >
        {emojify(whichEmoji(props.obj.optionId), options)}
        {props.obj.text}
      </Button>
    : null;
};

class ButtonExampleToggle extends Component {
  state = {};

  handleClick = e => {
    this.setState({
      [e]: !this.state[e]
    });
  };
  render() {
    const { woman, man, other } = this.state;

    const questions = this.props.app.surveyData.questions;

    const obj = questions.filter(x => x.questionId === "8")[0];
    console.log(questions);
    console.log(obj);
    return (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <Header as="h3">
          Kerroit kokeilleesi kannabista. Minkälaisia vaikutuksia sillä on sinuun? Jos mikään kuvauksista ei osu juuri kohdalleen, valitse se joka sopii parhaiten tai jätä kokonaan vastaamatta valitsemalla suoraan alapuolelta "OK". Voit myös valita useita vaihtoehtoja.
          {" "}
        </Header>
        {obj.options.map((e, i) => (
          <ButtonGenerator
            key={i}
            obj={e}
            active={this.state[e.optionId]}
            func={this.handleClick}
          />
        ))}

        <Button
          className="ui primary button"
          style={{ margin: "10px", alignSelf: "center" }}
          onClick={() => {
            let actives = [];
            forOwn(
              this.state,
              (value, key) => (value === true ? actives.push(key) : null)
            );

            this.props.registerMoods(actives);
          }}
        >
          OK
        </Button>
      </div>
    );
  }
}

export default ButtonExampleToggle;
