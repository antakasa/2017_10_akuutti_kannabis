import React from "react";
import { Icon, Image, Statistic } from "semantic-ui-react";
import { emojify } from "react-emojione";

const options = {
  convertShortnames: true,
  convertUnicode: true,
  convertAscii: true,
  style: {
    height: 50,
    margin: 4
  }
};

export const whichEmoji = optionId => {
  let emoji;
  switch (optionId) {
    case "13":
      emoji = ":scream:";
      break;
    case "11":
      emoji = ":relaxed:";
      break;
    case "12":
      emoji = ":expressionless:";
      break;
    case "10":
      emoji = ":stuck_out_tongue_winking_eye:";
      break;
    case "14":
      emoji = ":muscle:";
      break;
    default:
      return;
  }
  return emoji;
};

const statGenerator = (e, total, i) => {
  return e.optionId !== "15"
    ? <Statistic key={i} style={{ marginLeft: "20px" }}>
        <Statistic.Value>
          {emojify(whichEmoji(e.optionId), options)}
          {Math.round(parseInt(e.count, 10) / total * 100)}%
        </Statistic.Value>
        <Statistic.Label>{e.text}</Statistic.Label>
      </Statistic>
    : null;
};

const StatisticExampleEvenlyDivided = props => {
  let questions = props.app.surveyData.questions;
  if (!props.app.emojisVisible) {
    setTimeout(() => props.emojisVisible(), 1000);
  }
  let obj = questions.filter(x => x.questionId === "8")[0];

  let total = 0;
  obj.options.filter(
    x =>
      x.optionId === "13" ||
        x.optionId === "11" ||
        x.optionId === "12" ||
        x.optionId === "10" ||
        x.optionId === "14"
        ? (total = total + parseInt(x.count, 10))
        : null
  );

  return (
    <Statistic.Group widths="four">
      {obj.options.map((e, i) => statGenerator(obj.options[i], total, i))}

    </Statistic.Group>
  );
};

export default StatisticExampleEvenlyDivided;
