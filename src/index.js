import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Provider } from "react-redux";
import registerServiceWorker from "./registerServiceWorker";
import SortableComponent from "./sortList.js";
import store from "./store.js";
import staticData from "./data.json";
import {
  FourthQuestionApp,
  ThirdQuestionApp,
  SecondQuestionApp,
  App
} from "./App";
const SortApp = () => (
  <Provider store={store}>
    <SortableComponent />
  </Provider>
);

const ThirdQuestionAppProvider = () => (
  <Provider store={store}>
    <ThirdQuestionApp />
  </Provider>
);

const FourthQuestionAppProvider = () => (
  <Provider store={store}>
    <FourthQuestionApp />
  </Provider>
);

const SecondQuestionAppProvider = () => (
  <Provider store={store}>
    <SecondQuestionApp />
  </Provider>
);

const OtherApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

async function fetchAsync() {
  //let googleId = "1oeWQeQonFtOmjKjcgAv-I_29q9OT2FCaqLDwf6JZReU";
  //// await response of fetch call
  //let response = await fetch("http://localhost:6006/" + googleId);
  //// only proceed once promise is resolved
  //let data = await response.json();
  //// only proceed once second promise is resolved
  return staticData;
}

fetchAsync().then(data => {
  store.dispatch({ type: "UPDATE_DATA", data: staticData });

  document.addEventListener("DOMContentLoaded", function(event) {
    ReactDOM.render(<OtherApp />, document.getElementById("first_question"));

    ReactDOM.render(
      <ThirdQuestionAppProvider />,
      document.getElementById("third_question")
    );

    ReactDOM.render(
      <FourthQuestionAppProvider />,
      document.getElementById("fourth_question")
    );

    ReactDOM.render(
      <SecondQuestionAppProvider />,
      document.getElementById("second_question")
    );

    ReactDOM.render(<SortApp />, document.getElementById("sort"));
  });
});

registerServiceWorker();
