export const fetchGetJSON = func => {
  async function fetchAsync() {
    // await response of fetch call
    let response = await fetch(
      "https://survey.yle.fi/api/surveys/920/numerical-statistics"
    );
    // only proceed once promise is resolved
    let data = await response.json();
    // only proceed once second promise is resolved
    return data;
  }

  fetchAsync()
    .then(data => func(data.data))
    .catch(reason => console.log(reason.message));

  // trigger async function
  // log response or catch error of fetch promise
};

export function sendSurveyResults(data) {
  let request = new XMLHttpRequest();
  request.open("POST", "https://survey.yle.fi/api/responses/", true);
  request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
  request.send(JSON.stringify(data));
}
