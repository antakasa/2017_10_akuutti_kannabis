import React, { Component } from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  SortableContainer,
  SortableElement,
  arrayMove
} from "react-sortable-hoc";

import * as actionCreators from "./actions/actionCreators.js";
import { Table, Header, List, Button } from "semantic-ui-react";
const SortableItem = SortableElement(({ value }) => {
  return (
    <Table.Row>
      <Table.Cell style={{ textAlign: "center", cursor: "move" }}>
        {value}
      </Table.Cell>
    </Table.Row>
  );
});

export function mapStatetoProps(state) {
  return {
    app: state.app
  };
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actionCreators
    },
    dispatch
  );
}

const SortableList = SortableContainer(({ items }) => {
  return (
    <Table>
      <Table.Body>
        {items.map((value, index) => (
          <SortableItem key={`item-${index}`} index={index} value={value} />
        ))}
      </Table.Body>
    </Table>
  );
});

const fetchGetJSON = (url, headers) => {
  console.log(url);
  return fetch(
    "https://survey-test.yle.fi/api/surveys/${surveyId}/statistics",
    {
      method: "GET",
      headers: {
        "x-api-key": "c05d7f7a299c6ca42879c6e649c341235af638d9d9867ba59aca26dd9530ddb7"
      },
      credentials: "same-origin"
    }
  )
    .then(({ data }) => console.log(data))
    .catch(function(error) {
      console.log(
        "There has been a problem with your fetch operation: " + error
      );
    });
};

export function fetchSurveyResults(surveyId) {
  const apiKey =
    "c05d7f7a299c6ca42879c6e649c341235af638d9d9867ba59aca26dd9530ddb7";
  const surveyEndpointUrl = `https://survey-test.yle.fi/api/surveys/${surveyId}/statistics`;
  const headers = { "x-api-key": apiKey };
  return fetchGetJSON(surveyEndpointUrl, "same-origin", headers);
}
export function sendSurveyResults(data) {
  let request = new XMLHttpRequest();
  request.open("POST", "https://survey-test.yle.fi/api/responses/", true);
  request.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
  request.send(JSON.stringify(data));
}

const expertItems = [
  "alkoholi",
  "tupakka",
  "amfetamiini",
  "kannabis",
  "anaboliset steroidit",
  "buprenorfiini (mm. Subutex)"
];

const ListExampleOrdered = props => (
  <div style={{ display: "flex", flexDirection: "row" }}>
    <List ordered>
      <p>Sinä arvioit:</p>
      {props.items.map(e => <List.Item>{e}</List.Item>)}
    </List>

    <List ordered style={{ marginTop: 0, marginLeft: "4%" }}>
      <p>Tutkimustieto:</p>

      {expertItems.map(e => <List.Item>{e}</List.Item>)}
    </List>
  </div>
);

class SortableComponent extends Component {
  state = {
    items: [
      "alkoholi",
      "tupakka",
      "amfetamiini",
      "kannabis",
      "anaboliset steroidit",
      "buprenorfiini (mm. Subutex)"
    ]
  };
  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState({
      items: arrayMove(this.state.items, oldIndex, newIndex)
    });
  };

  onDone = () => {
    let surveyId = 103;
    const arr = this.state.items;
    let answers = arr.map((e, i) => {
      return { answer: e, questionId: i };
    });
    let obj = {
      date: new Date().toString(),
      surveyId: surveyId,
      externalData: {},
      answers: answers
    };
    console.log(obj);
  };
  render() {
    return (
      <div>
        {typeof this.props.app.sorted == "undefined"
          ? <div style={{ maxWidth: "800px" }}>
              <SortableList
                helperClass={"sort_helper"}
                items={this.state.items}
                onSortEnd={this.onSortEnd}
                hideSortableGhost={true}
                lockAxis={"y"}
              />
              <Button
                primary
                onClick={() => this.props.registerSort(this.state.items)}
                content="Valmis"
              />
            </div>
          : <div>

              <ListExampleOrdered items={this.state.items} />
            </div>}
      </div>
    );
  }
}

const App = connect(mapStatetoProps, mapDispatchToProps)(SortableComponent);

export default App;
