import React from "react";
import CountTo from "react-count-to";

import { Statistic } from "semantic-ui-react";

const fn = value => <span>{value}</span>;
const BigNumber = props => (
  <Statistic style={{ width: "300px" }}>
    <Statistic.Value>
      <CountTo to={props.value} speed={1000} onComplete={() => null}>
        {fn}
      </CountTo>%
    </Statistic.Value>
    <Statistic.Label>{props.text}</Statistic.Label>
  </Statistic>
);

export default BigNumber;
