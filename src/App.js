import React, { Component } from "react";
import logo from "./logo.svg";
import { Button, Message, Header, Segment } from "semantic-ui-react";
import "./semantic.min.css";
import "./App.css";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actionCreators from "./actions/actionCreators.js";
import ShowContentBasedOnAge from "./showContentBasedOnAge.js";
import AgeDrop from "./ageDropdown.js";
import YesOrNo from "./hotOrNot.js";
import GenderSelection from "./selectGender.js";
import {
  BarChart,
  Text,
  Cell,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  Legend,
  Bar
} from "recharts";
import { fetchGetJSON, sendSurveyResults } from "./fetchOperations.js";
import DOMPurify from "dompurify";
import inRange from "in-range";
import EmojiStats from "./emojiStastistics.js";
import ChooseMood from "./selectMood.js";
import IconList from "./icons.js";
import BigNumber from "./BigNumber.js";

const data_who_accepts = [
  { name: "15-24", data: 24 },
  { name: "23-34", data: 29 },
  { name: "35-44", data: 16 },
  { name: "45-69", data: 6 }
];

const data_who_tried = [
  { name: "15-24", data: 23, miehet: 24, naiset: 22 },
  { name: "23-34", data: 38, miehet: 44, naiset: 32 },
  { name: "35-44", data: 25, miehet: 32, naiset: 18 },
  { name: "45-69", data: 9, miehet: 13, naiset: 6 }
];

const dataDetermineGroup = age => {
  let group;
  if (inRange(age, 15, 24)) group = "15-24";
  else if (inRange(age, 23, 34)) group = "23-34";
  else if (inRange(age, 35, 44)) group = "35-44";
  else if (inRange(age, 45, 69)) group = "45-69";
  return group;
};

const DrawBarChart = props => {
  const group = dataDetermineGroup(parseInt(props.age, 10));
  return (
    <div style={{ maxWidth: "400px", marginBottom: "65px" }}>
      <ResponsiveContainer width={"100%"} height={400}>
        <BarChart data={props.data}>
          <XAxis dataKey="name" />
          <YAxis domain={[0, 100]} />
          <Legend
            verticalAlign="bottom"
            height={36}
            content={
              <small>
                {props.label}
                {" "}
                <a target="_blank" href={props.source}>
                  Lähde.
                </a>
              </small>
            }
          />
          <YAxis />
          <Tooltip />
          <CartesianGrid strokeDasharray="3 3" />
          <Bar dataKey="data" label fill="#00809f">
            {props.data.map((entry, index) => {
              const color = entry.name === group ? "#f0730f" : "#007882";
              return <Cell key={index} fill={color} />;
            })}
          </Bar>

        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};

function mapStatetoProps(state) {
  return {
    app: state.app
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actionCreators
    },
    dispatch
  );
}

const InputBasicInfo = props => {
  return (
    <div>
      <div id="basicInfoStarts">
        <Header as="h3">Valitse ikä</Header>
        <AgeDrop {...props} />
        <Header as="h3">Sukupuoli</Header>
        <GenderSelection func={e => props.registerGender(e)} />

        <Header as="h3">Oletko koskaan kokeillut kannabista?</Header>
        <YesOrNo
          fOption="Kyllä"
          sOption="En"
          func={e => props.registerTry(e)}
        />
      </div>
      {props.app.gender && props.app.age && props.app.tryed
        ? <div className="basic_info_result">
            Kerroit olevasi

            {" "}
            {props.app.age === 15 ? "15-vuotias tai nuorempi" : null}

            {props.app.age === 65 ? "65-vuotias tai vanhempi" : null}

            {props.app.age < 65 && props.app.age > 15
              ? props.app.age + "-vuotias"
              : null}
            {" "}
            {props.app.gender === "mies" || props.app.gender === "nainen"
              ? props.app.gender
              : "muu-sukupuolinen"}
            ,
            joka
            {" "}
            {props.app.tryed === "yes"
              ? "on kokeillut kannabista"
              : "ei ole kokeillut kannabista."}

            {" "} Jatketaanko?{" "}
            <div>
              <Button
                className="ui primary button"
                style={{ marginTop: "10px" }}
                onClick={() => {
                  props.registerBasicInfo();
                }}
              >
                OK
              </Button>
            </div>
          </div>
        : <div>
            Näet tuloksesi kun valitset ikäsi, sukupuolesi ja vastaat kysymykseen, oletko kokeillut kannabista.
            {" "}
          </div>}
    </div>
  );
};

class App_ extends Component {
  state = { open: false };

  componentWillMount() {
    fetchGetJSON(this.props.updateSurveyResults);
  }

  componentDidUpdate() {
    if (
      this.props.app.basicInfoRegistered &&
      !this.props.app.emojisVisible &&
      typeof this.props.app.moods === "undefined" &&
      typeof this.props.app.netherLandsOrnot === "undefined" &&
      typeof this.props.app.medicineOrNot === "undefined"
    ) {
      //setTimeout(() => {
      //document
      //.getElementById("basicInfo")
      //.scrollIntoView({ behavior: "smooth", block: "start" });
      //return false;
      //}, 50);
    }
    if (
      this.props.app.tryed === "yes" &&
      typeof this.props.app.moods !== "undefined" &&
      typeof this.props.app.netherlandsOrNot === "undefined" &&
      typeof this.props.app.medicineOrNot === "undefined"
    ) {
      //document
      //.getElementById("emoji_block")
      //.scrollIntoView({ behavior: "smooth" });
    }
  }
  render() {
    return (
      <div className="App" id="App">

        <div name="basicInfo" id="basicInfo">
          {this.props.app.basicInfoRegistered
            ? <div className="segment_container">

                {this.props.app.age === 15
                  ? <Header as="h4">
                      Lähin ikäryhmäsi on
                      {" "}
                      {dataDetermineGroup(this.props.app.age)}
                      -vuotiaat.

                    </Header>
                  : null}

                {this.props.app.age === 65
                  ? <Header as="h4">
                      Lähin ikäryhmäsi on
                      {" "}
                      {dataDetermineGroup(this.props.app.age)}
                      -vuotiaat.

                    </Header>
                  : null}
                <ShowContentBasedOnAge
                  data={this.props.app.data.question1}
                  age={this.props.app.age}
                />

                <DrawBarChart
                  label={
                    "Kuvio: Kannabista kokeilleet ikäryhmittäin (%). Oranssi palkki kuvaa omaa ikäryhmääsi."
                  }
                  data={data_who_tried}
                  source={
                    "http://www.julkari.fi/bitstream/handle/10024/126845/hakkarainen.pdf?sequence=4"
                  }
                  age={this.props.app.age}
                />

                <IconList
                  data={data_who_tried}
                  gender={this.props.app.gender}
                  group={dataDetermineGroup(this.props.app.age)}
                />
                {this.props.app.data.question1.toAll.paragraphs.map((e, i) => (
                  <p
                    key={i}
                    dangerouslySetInnerHTML={{
                      __html: DOMPurify.sanitize(e.value)
                    }}
                  />
                ))}

                <div id="emoji_block">
                  {this.props.app.tryed === "yes" &&
                    typeof this.props.app.moods === "undefined"
                    ? <ChooseMood {...this.props} />
                    : <div>
                        <Header as="h4">
                          Näin he, jotka kannabista ovat kokeilleet, kuvailevat sen vaikutuksia. Aineisto koostuu tämän artikkelin lukijoiden vastauksista:
                          {" "}
                        </Header>

                        <EmojiStats {...this.props} />
                      </div>}
                </div>
              </div>
            : <InputBasicInfo {...this.props} />}
        </div>
      </div>
    );
  }
}

class SecondQuestion extends Component {
  state = { open: false };

  render() {
    return (
      <div className="App" id="App">
        {typeof this.props.app.legalOrNot !== "undefined"
          ? <div>
              <p>
                Vastasit
                {" "}
                {this.props.app.legalOrNot === "yes" ? "kyllä." : "ei."}
              </p>
              <ShowContentBasedOnAge
                data={this.props.app.data.question2}
                age={this.props.app.age}
                yesOrNo={this.props.app.legalOrNot}
              />

              <DrawBarChart
                label={
                  "Kuvio: Myönteisyys kannabiksen laillistamiseen ikäryhmittäin (%). Pelkän lääkekäytön hyväksyvät tulkittu kielteisiksi."
                }
                data={data_who_accepts}
                source={
                  "http://www.julkari.fi/bitstream/handle/10024/126845/hakkarainen.pdf?sequence=4"
                }
                age={this.props.app.age}
              />
              <p>
                THL:n vuonna 2014 teettämän
                {" "}
                <a href="http://www.julkari.fi/bitstream/handle/10024/126845/hakkarainen.pdf?sequence=4">
                  {" "}huumekyselyn
                </a>
                {" "}
                mukaan kannabiksen täyttä laillistamista kannattaa noin 15 prosenttia koko väestöstä. Kannabista käyttävien keskuudessa vastaava luku on merkittävästi isompi: noin 70 prosenttia heistä kannattaa kannabiksen täyttä laillistamista.
              </p>
            </div>
          : <div>
              <YesOrNo
                fOption={"Kyllä"}
                sOption={"Ei"}
                func={e => this.props.legalOrNot(e)}
              />
            </div>}
      </div>
    );
  }
}

const ThirdQuestionResult = props => {
  const questions = props.app.surveyData.questions;
  const count = props.app.surveyData.count;
  const obj = questions.filter(x => x.questionId === "29")[0];

  const opt = obj.options.filter(x => x.optionId === "30")[0];

  let total = 0;
  obj.options.filter(
    x =>
      x.optionId === "30" || x.optionId === "31"
        ? (total = total + x.count)
        : null
  );

  return (
    <div>
      <BigNumber
        value={opt.count / total * 100}
        text={"vastanneista kannattaa Hollannin mallia"}
      />
      <p>
        Vastanneet koostuvat artikkelin lukijoista. Mukana ovat kysymykseen annetut vastaukset sillä hetkellä, kun avasit artikkelin (
        {total}
        {" "}
        kpl).
        {" "}
        {" "}
      </p>
    </div>
  );
};

class ThirdQuestion extends Component {
  state = { open: false, answerSent: false };
  componentDidUpdate() {
    const moodAnswerGenerator = () => {
      if (
        typeof this.props.app.moods === "undefined" ||
        this.props.app.moods.length === 0
      )
        return [];
      else
        return this.props.app.moods.map(e => {
          return { questionId: 8, optionId: parseInt(e, 10) };
        });
    };

    const netherlandsGenerator = () => {
      let returnObj;
      if (this.props.app.netherlandsOrNot === "yes")
        returnObj = [{ optionId: 30, questionId: 29 }];
      else returnObj = [{ optionId: 31, questionId: 29 }];
      return returnObj;
    };

    if (
      (this.props.app.netherlandsOrNot === "yes" ||
        this.props.app.netherlandsOrNot === "no") &&
      !this.state.answerSent
    ) {
      const moodResults = moodAnswerGenerator();
      const netherlandsResults = netherlandsGenerator();
      let answers = [...moodResults, ...netherlandsResults];
      let surveyId = 920;
      let obj = {
        date: new Date().toString(),
        surveyId: surveyId,
        externalData: {},
        answers: answers
      };
      sendSurveyResults(obj);
      this.setState({ answerSent: true });
    }
  }

  render() {
    return (
      <div className="App" id="App">
        {typeof this.props.app.netherlandsOrNot !== "undefined"
          ? <div>
              <p>
                Vastasit
                {" "}
                {this.props.app.netherlandsOrNot === "yes" ? "kyllä." : "ei."}
              </p>
              <ThirdQuestionResult {...this.props} />
            </div>
          : <div>
              <YesOrNo
                fOption={"Kyllä"}
                sOption={"Ei"}
                func={e => this.props.netherlandsOrNot(e)}
              />
            </div>}
      </div>
    );
  }
}

class FourthQuestion extends Component {
  state = { open: false };

  render() {
    return (
      <div className="App" id="App">
        {typeof this.props.app.medicineOrNot !== "undefined"
          ? <div>
              <p>
                Vastasit
                {" "}
                {this.props.app.medicineOrNot === "yes" ? "kyllä." : "ei."}
              </p>
              {this.props.app.medicineOrNot === "yes"
                ? <div>
                    <p>
                      Kuulut suomalaisten enemmistöön. Viimeisimmän tutkimuksen mukaan suomalaisista
                    </p>

                    <BigNumber value={66} text={"hyväksyy lääkekannabiksen."} />
                  </div>
                : <div>
                    <p>
                      Kuulut suomalaisten vähemmistöön. Viimeisimmän tutkimuksen mukaan suomalaisista
                      {" "}
                      {" "}
                    </p>

                    <BigNumber value={66} text={"hyväksyy lääkekannabiksen."} />
                  </div>}

            </div>
          : <div>
              <YesOrNo
                fOption={"Hyväksyn"}
                sOption={"En hyväksy"}
                func={e => this.props.medicineOrNot(e)}
              />
            </div>}
      </div>
    );
  }
}

const FourthQuestionApp = connect(mapStatetoProps, mapDispatchToProps)(
  FourthQuestion
);

const ThirdQuestionApp = connect(mapStatetoProps, mapDispatchToProps)(
  ThirdQuestion
);

const SecondQuestionApp = connect(mapStatetoProps, mapDispatchToProps)(
  SecondQuestion
);

const App = connect(mapStatetoProps, mapDispatchToProps)(App_);

export { App, SecondQuestionApp, ThirdQuestionApp, FourthQuestionApp };
