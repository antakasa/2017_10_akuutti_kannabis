export function registerAge(num) {
  return {
    type: "REGISTER_AGE",
    age: num
  };
}

export function emojisVisible() {
  return {
    type: "EMOJIS_VISIBLE"
  };
}

export function registerSort(array) {
  return {
    type: "REGISTER_SORT",
    sort: array
  };
}

export function updateSurveyResults(data) {
  return {
    type: "UPDATE_SURVEY_RESULTS",
    surveyData: data
  };
}

export function netherlandsOrNot(string) {
  return {
    type: "NETHERLANDS_OR_NOT",
    netherlandsOrNot: string
  };
}

export function medicineOrNot(string) {
  return {
    type: "MEDICINE_OR_NOT",
    medicineOrNot: string
  };
}

export function legalOrNot(string) {
  return {
    type: "LEGAL_OR_NOT",
    legalOrNot: string
  };
}

export function registerMoods(array) {
  return {
    type: "REGISTER_MOODS",
    moods: array
  };
}
export function registerGender(gender) {
  return {
    type: "REGISTER_GENDER",
    gender: gender
  };
}

export function registerBasicInfo() {
  return {
    type: "REGISTER_BASIC_INFO",
    bool: true
  };
}

export function registerTry(tryed) {
  return {
    type: "REGISTER_TRY",
    tryed: tryed
  };
}

export function updateData(data) {
  return {
    type: "UPDATE_DATA",
    data: data
  };
}
